import tornado.ioloop
import tornado.web

from internal.handler.get_rewrite_content import GetRewriteContentHandler
from internal.task.rewrite import Rewrite

def make_app():
    rewrite_ins = Rewrite(None, None, None)
    vocab, matrix, d_lookup = rewrite_ins.get_resources()
    print 'Finish loading resources'

    return tornado.web.Application([
        (r"/get_rewrite_content", GetRewriteContentHandler, dict(vocab=vocab, matrix=matrix, d_lookup=d_lookup))
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(7000)
    tornado.ioloop.IOLoop.current().start()
