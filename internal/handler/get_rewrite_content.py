import tornado.ioloop
import tornado
import json

from internal.task.rewrite import Rewrite


class GetRewriteContentHandler(tornado.web.RequestHandler):
    def initialize(self, vocab, matrix, d_lookup):
        self.__rewrite_ins = Rewrite(vocab, matrix, d_lookup)

    def post(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        content = self.get_argument('content', '')
        d_result = {
            'content': self.__rewrite_ins.rewrite(content),

        }

        self.write(json.dumps(d_result))

