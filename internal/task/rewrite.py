# -*- coding: utf-8 -*-

import json
import cPickle as pkl

from gensim.models import KeyedVectors
from pyvi import ViTokenizer, ViPosTagger


class Rewrite:
    def __init__(self, vocab, matrix, d_lookup):
        self.__vocab = vocab
        self.__matrix = matrix
        self.__d_lookup = d_lookup
        print "Finish intialize model"

    def get_resources(self):
        with open('./data/vocab.pkl', 'r') as f:
            vocab = pkl.load(f)

        with open('./data/count_matrix_reduce.pkl', 'r') as f:
            matrix = pkl.load(f)

        d_lookup = self.build_lookup_dict('./data/word_net.json')
        return vocab, matrix, d_lookup

    def build_lookup_dict(self, word_net_path):
        d_lookup = {}

        with open('data/word_net.json', 'r') as f:
            l_line = f.readlines()

        for line in l_line:
            if line.strip() == '':
                continue

            line = json.loads(line)
            raw_sense_word = line['sense_words']
            l_sense_word = raw_sense_word.split(',')
            for sense_word in l_sense_word:
                sense_word = sense_word.strip()
                for sense_word_2 in l_sense_word:
                    sense_word_2 = sense_word_2.strip()
                    if sense_word_2 == sense_word:
                        continue
                    if sense_word not in d_lookup:
                        d_lookup[sense_word] = []
                    d_lookup[sense_word].append(sense_word_2)

        return d_lookup

    def get_prob(self, cur_word, pre_word, suf_word):
        word = pre_word + ' ' + cur_word + ' ' + suf_word
        word = word.strip()
        if word not in self.__vocab:
            return 0.0
        else:
            idx = self.__vocab[word]
            return self.__matrix[0, idx]

    def rewrite(self, txt):
        l_line = txt.split('\n')
        l_rewrite_token = []

        d_replace = {}
        for line in l_line:
            l_word = ViPosTagger.postagging(ViTokenizer.tokenize(line))
            l_token = l_word[0]
            l_pos = l_word[1]

            for idx, token in enumerate(l_token):
                if token.strip() == '':
                    continue
                normalized_token = token.replace('_', ' ')

                if normalized_token in self.__d_lookup:
                    l_candidate_token = self.__d_lookup[normalized_token]
                else:
                    l_candidate_token = []

                if len(l_candidate_token) > 0:
                    if l_pos[idx] not in ('R', 'A', 'C','E') or idx == 0 or idx == (len(l_token) - 1):
                        l_rewrite_token.append(token.replace('_', ' '))
                        continue

                    highest_prob = -1
                    highest_pos = -1
                    for pos, candidate_token in enumerate(l_candidate_token):
                        candidate_token = candidate_token.strip().replace(' ', '_')
                        cur_prob = self.get_prob(candidate_token, l_token[idx - 1], l_token[idx + 1])
                        if cur_prob > highest_prob:
                            highest_pos = pos
                            highest_prob = cur_prob

                    replace_token = l_candidate_token[highest_pos]
                    d_replace[l_token[idx - 1] + ' ' + normalized_token + ' ' + l_token[idx + 1]] = l_token[idx - 1] + ' ' + replace_token.strip() + ' ' + l_token[idx + 1]

                    l_rewrite_token.append(replace_token.replace('_', ' '))
                else:
                    l_rewrite_token.append(token.replace('_', ' '))

            l_rewrite_token.append('\n')

        for ances_token in d_replace:
            txt = txt.replace(ances_token, d_replace[ances_token])
        return txt


