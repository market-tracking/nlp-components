import os
import cPickle as pkl

from sklearn.feature_extraction.text import CountVectorizer

def get_list_file_path(directory, l_except_file_name=['.DS_Store']):
    stripped_dir = directory.rstrip('/')
    return ['{}/{}'.format(stripped_dir, file_name) for file_name in os.listdir(stripped_dir) if file_name not in l_except_file_name]


def get_list_file_name(directory, l_except_file_name=['.DS_Store']):
    stripped_dir = directory.rstrip('/')
    return [file_name for file_name in os.listdir(stripped_dir) if file_name not in l_except_file_name]

DIR = './test/'
l_file_path = get_list_file_path(DIR)

count_vector = CountVectorizer(input='filename', ngram_range=(1, 2))
X = count_vector.fit_transform(l_file_path)


with open('count_matrix.pkl', 'w+') as f:
    pkl.dump(X, f)

with open('vocab.pkl', 'w+') as f:
    pkl.dump(count_vector.vocabulary_, f)

