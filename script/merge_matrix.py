import os
import cPickle as pkl


def get_list_file_path(directory, l_except_file_name=['.DS_Store']):
    stripped_dir = directory.rstrip('/')
    return ['{}/{}'.format(stripped_dir, file_name) for file_name in os.listdir(stripped_dir) if file_name not in l_except_file_name]


def get_list_file_name(directory, l_except_file_name=['.DS_Store']):
    stripped_dir = directory.rstrip('/')
    return [file_name for file_name in os.listdir(stripped_dir) if file_name not in l_except_file_name]

with open('count_matrix.pkl', 'r') as f:
    matrix = pkl.load(f)

with open('count_matrix_reduce.pkl', 'w+') as f:
    pkl.dump(matrix.sum(axis=0), f)
