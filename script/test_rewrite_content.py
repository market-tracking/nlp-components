# -*- coding: utf-8 -*-

from internal.task.rewrite import Rewrite

txt = u"""
<p>Tính tới cuối tháng 7, <a href="http://batdongsan.com.vn/goi-ho-tro-30000-ty-dong">gói hỗ trợ 30.000 tỷ</a> của Chính phủ đã đi vào hoạt động tròn 2 tháng. Do thủ tục và điều kiện được vay khá phức tạp nên tính đến nay, mới chỉ có gần 60 trường hợp tiếp cận được. Dù chưa có sự ảnh hưởng rõ rệt tới thị trường BĐS nhưng những dự án nằm trong gói hỗ trợ này vẫn nhận được sự quan tâm lớn từ phía khách hàng, đặc biệt là những khách hàng bình dân đang có nhu cầu mua nhà để ở.<br>
<br>
Ở phân khúc căn hộ chung cư, tình hình giao dịch trong tháng giữa Hà Nội và Tp.HCM có sự khác biệt lớn. Hầu hết các dự án có giao dịch khả quan thuộc về khu vực <strong>Tp.HCM</strong>. Có thể kể đến tên một số dự án giá rẻ và tầm trung như: chung cư Phạm Văn Hai (Q.Tân Bình); dự án Res III (Q.7); chung cư Khang Gia Tân Hương (Q.Tân Phú); căn hộ Hưng Phát (Q.7); dự án Petro Land Mark (Q.2)...</p>
<p>Ở mảng căn hộ chung cư cao cấp, thị trường Tp.HCM cũng cho thấy những chuyển biến nhẹ theo hướng tích cực. Tiêu biểu như dự án Lakeview (Q.Bình Thạnh) với giá từ 1,9 đến 5 tỷ/căn đã bán hết trong vòng 4 tháng. Một số dự án chung cư cao cấp khác như Sunrise City (Q.7), Tropic Garden (Q.2) vẫn bán được, tuy nhiên nguồn cầu lại khá khiêm tốn. Dự án Imperian An Phú (Q.2) do có pháp lý tốt, chính sách ưu đãi nên hiện chỉ còn hơn 20 căn diện tích lớn từ 131-135 m2...</p>
<table align="center" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td><img alt="" title="Thị trường BĐS tháng 7/2013: Ấm lên ở một vài phân khúc phía Nam " src="http://file1.batdongsan.com.vn/file.302372.jpg" style="width: 500px; height: 228px;"></td>
</tr>
<tr>
<td style="text-align: center;"><span style="color:#696969;"><span style="font-size: 11px;">Ảnh minh họa (Nguồn: Internet)</span></span></td>
</tr>
</tbody>
</table>
<p>Ông Võ Thanh Hùng, trưởng phòng kinh doanh sàn giao dịch Công ty BĐS Quốc tế và Cộng sự, cho biết, những dự án này bán được là do hầu như đang trong giai đoạn xây dựng hoàn thiện hoặc sẽ bàn giao nhà trong năm tới. Đồng thời, một số dự án thuộc diện được vay vốn trong gói hỗ trợ 30.000 tỷ của Chính phủ cũng nhận được sự quan tâm lớn từ phía những khách hàng bình dân.<br>
<br>
Ngoài ra, thông tin giảm giá của nhiều dự án tại Tp.HCM như Phước Long Spring Town (Q.9); căn hộ cao cấp Phúc Yên 2 (Q.Tân Bình); căn hộ Dragon Hill Residence and Suites; dự án Hưng Phát (H.Nhà Bè)… cũng là những tín hiệu tốt cho người mua. Đây có thể coi là một trong những nguyên nhân chính khiến tình hình giao dịch tại khu vực phía Nam “ấm” lên trong tháng 7.<br>
<br>
Trái ngược với thị trường Tp.HCM, giao dịch căn hộ tại khu vực <strong>Hà Nội</strong> lại chậm hẳn so với tháng 6, cả ở phân khúc căn hộ bình dân và cao cấp. Anh Vũ, nhân viên kinh doanh của Cen Group cho biết, giao dịch tháng này chỉ bằng 50% tháng 6. Một số nhân viên các sàn giao dịch BĐS tại Hà Nội như V.I.C, Vũ Gia, Vland… cũng khẳng định giao dịch tháng 7 kém hơn hẳn so với tháng 6 và một vài tháng trước đó. Mảng căn hộ chung cư dưới 2 tỷ như dự án OCT2 Xuân Phương; chung cư CT3 Hoàng Quốc Việt Residential (H.Từ Liêm) là những điểm sáng lẻ loi trên thị trường...<br>
<br>
Các giao dịch lẻ từ một số dự án cao cấp do nhà đầu tư cắt lỗ tại các chung cư Times City (Q.Hai Bà Trưng); Royal City; Hapulico (Q.Thanh Xuân)… diễn ra lác đác. Đây đều là các dự án cao cấp sắp và đang bàn giao nhà, hiện chủ đầu tư đã bán hết. Giao dịch trên thị trường chỉ là giữa các nhà đầu tư với nhau hoặc giữa nhà đầu tư với người dân.<br>
<br>
Theo lý giải của một số lãnh đạo sàn giao dịch BĐS thì tâm lý chờ được giải ngân từ gói 30.000 tỷ của khách hàng khu vực miền Bắc rõ rệt hơn hẳn phía Nam. Đồng thời, sự kỳ vọng vào nhà ở xã hội và nhà giá rẻ của người dân cũng là nguyên nhân khiến giao dịch trên thị trường chững lại. Một số dự án có sự nhích nhẹ về giá cũng đã khiến khách hàng đắn đo hơn trước khi quyết định xuống tiền mua nhà. Tiêu biểu như chung cư Rainbow Linh Đàm (Q.Hoàng Mai) sau khi đạt được lượng căn hộ bán ra thì chủ đầu tư đã quyết định tăng giá theo kế hoạch đã định. Dự án Sail Tower (Q.Hà Đông) cũng tăng nhẹ thêm 100.000 đồng/m2. Dự án CT3 Cổ Nhuế (H.Từ Liêm) tăng từ 22 triệu lên 24 triệu/m2 nên giao dịch chậm hẳn.<br>
<br>
Dù giá BĐS đã giảm khá sâu so với mức đỉnh nhưng nhiều nhà đầu tư cho rằng thị trường vẫn còn đang nằm trong thế giằng co, chưa phải thời điểm tốt để đầu tư. Do đó, rất ít người tham gia đầu tư vào địa ốc thời điểm này, trừ một số bộ phận môi giới “lướt sóng” ở các dự án giảm giá mạnh so với mặt bằng chung như VP5 Linh Đàm. Dù “hiện tượng” VP5 Linh Đàm chỉ là trường hợp đặc biệt, không phản ánh đúng được diện mạo chung của thị trường nhưng “sức hút” từ phía dự án này đã cho thấy nhu cầu của người dân về nhà ở vẫn vô cùng lớn. Khi dự án có giá hợp lý, vừa với túi tiền của người dân, vị trí đẹp và chất lượng tốt thì vẫn luôn được khách hàng quan tâm và có giao dịch tốt.</p>
<table style="width: 200px;" align="center" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td><img alt="" title="Thị trường BĐS tháng 7/2013: Ấm lên ở một vài phân khúc phía Nam 1" src="http://file1.batdongsan.com.vn/file.302296.jpg" style="width: 500px; height: 332px;"></td>
</tr>
<tr>
<td style="text-align: center;"><span style="color:#696969;"><span style="font-size: 11px;">Phân khúc đất nền phía Nam đang "ấm". (Ảnh minh họa)</span></span></td>
</tr>
</tbody>
</table>
<p>Tháng 7 cũng là khoảng thời gian phân khúc đất nền phía Nam tiếp tục ấm lên. Tại <strong>Tp.HCM</strong>, đất nền tại Hóc Môn, Nhà Bè bán khá tốt với khoảng 100 -110 nền/tháng. Dự án 4S Riverside Linh Đông (Q.Thủ Đức) từ ngày công bố thông tin đến ngày mở bán (28/7) cũng đã có hơn 100 khách hàng đặt chỗ. Tại <strong>Đồng Nai</strong>, dự án Lavender (Tp. Biên Hòa) giai đoạn 1 đã bán hết, hiện đang bán giai đoạn 2 và lượng giao dịch cũng rất khả quan. Đất nền dự án Green Life City (H.Trảng Bom) giai đoạn này cũng bán tốt hơn hẳn. Dự án Blue Topaz (H. Long Thành) mở bán từ tháng 9/2012 cho tới nay cũng chỉ còn lại 30%. Ở <strong>Hà Nội</strong>, đất nền khu vực ngoại thành như Gia Lâm, Cầu Diễn, Từ Liêm… cũng đã có lượng khách hàng quan tâm trở lại. Tuy lượng bán vẫn chậm nhưng đã có sự biến chuyển so với thời gian dài im ắng trước đó. Anh Ma Tú Phương, nhân viên sàn BĐS HTreal (Tp.HCM) nhận định, nguyên nhân phân khúc này “hút khách” trong giai đoạn hiện nay là do khách hàng đang mất niềm tin vào thị trường căn hộ, nhất là thực tế nhiều chung cư khi vào ở có nội thất kém, chất lượng không đạt yêu cầu nên khá nhiều người có tâm lý muốn mua đất nền rồi tự xây dựng. Trong khi đó, nguồn cung đất nền sẽ ngày càng ít đi, nên giới đầu tư cũng chú trọng nhiều hơn vào phân khúc này.<br>
<br>
Trao đổi với <a href="http://batdongsan.com.vn/">Batdongsan.com.vn</a> về tình hình thị trường BĐS từ nay đến cuối năm, ông Võ Thanh Hùng nhận định: “Khả năng giá sẽ còn giảm nhẹ do nguồn cung tiếp tục tăng khi các dự án dần hoàn thiện, mặt khác doanh nghiệp sau thời gian bám trụ, gặp nhiều khó khăn về tài chính sẽ buộc phải giảm giá để đẩy hàng.”</p>
"""
rewrite_ins = Rewrite()
print rewrite_ins.rewrite(txt)

txt = u"""
Thứ nhất, các chủ đầu tư sẽ gặp nhiều khó khăn trong việc triển khai dự án, đặc biệt là tại Tp.HCM. Trong chính sách mới về quy hoạch phát triển thành phố giai đoạn 2016-2025, nhằm giảm tải áp lực lên cơ sở hạ tầng, Tp.HCM chủ trương hạn chế phát triển các dự án thuộc khu vực nội đô hoặc khu vực có nhiều khu dân cư hiện hữu. Theo bà Dung, việc hạn chế này sẽ ảnh hưởng tới kế hoạch kinh doanh của doanh nghiệp có dự án thuộc khu trung tâm. Các doanh nghiệp sẽ không dễ dàng xin được giấy phép xây dựng.  

Thứ hai, các chủ đầu tư sẽ còn phải đối mặt với thách thức liên quan tới quỹ đất và giá đất. Quỹ đất tại những khu vực phát triển nóng của Hà Nội và Tp.HCM không còn nhiều khiến giá đất, đặc biệt là đất khu vực trung tâm thành phố tăng mạnh. Do đó, để phát triển bền vững, các chủ đầu tư cần nguồn vốn lớn khi triển khai dự án.

Thứ ba, một thách thức không mới nhưng rất khó giải quyết mà các chủ đầu tư bất động sản phải đối mặt là thách thức liên quan tới cơ sở hạ tầng. Ngập lụt hay tắc đường là hệ lụy bao lâu nay của sự thiếu đồng bộ cơ sở hạ tầng tại đô thị lớn. Thách thức này đang ngày càng nghiêm trọng hơn do số lượng người sở hữu xe ô tô, số lượng xe cộ lưu thông liên tục tăng, các dự án nhà ở liên tiếp nở rộ nhưng cơ sở hạ tầng hầu như không được cải thiện. Những dự án nằm trong “vùng trũng” này sẽ gặp nhiều vấn đề trong phát triển. Người mua đang ngày càng cẩn trọng hơn trong việc lựa chọn dự án.

Giám đốc cấp cao CBRE Việt Nam nhấn mạnh những thách thức trên sẽ có những tác động khác nhau đến từng phân khúc của thị trường. Đối với dòng sản phẩm cao cấp và hạng sang, thách thức này sẽ lớn hơn do thường nằm ở khu vực nội đô, khu vực trung tâm thành phố. Ở vị trí này, các dự án sẽ bị hạn chế nhiều hơn trong việc cấp phép xây dựng và cũng chịu những ảnh hưởng của tắc đường, ngập lụt nặng nề hơn. Trong khi đó, những dự án bình dân và trung cấp sẽ không bị ảnh hưởng nhiều do thường nằm ở khu vực ven thành phố, ven trung tâm.

Ông Cấn Văn Lực, chuyên gia kinh tế trưởng BIDV cũng chỉ ra 3 thách thức chính với thị trường bất động sản Việt Nam trong năm 2019. Trước hết, chiến tranh thương mại Mỹ - Trung dẫn đến động thái tăng lãi suất ở nhiều nước trên thế giới. Do đó, tình hình kinh tế vĩ mô trên thế giới trong năm 2019 được dự báo là tăng trưởng khó khăn hơn, nhiều rủi ro hơn và có thể là thấp hơn so với 2018. Chính vì vậy, Việt Nam được dự báo là sẽ gặp nhiều thách thức hơn, rủi ro hơn với các áp lực liên quan tới lạm phát, tỷ giá, lãi suất… Điều này ảnh hưởng trực tiếp đến nguồn vốn của các doanh nghiệp bất động sản.

Thách thức thứ 2 với thị trường bất động sản Việt Nam là tình trạng cung vượt cầu ở một vài phân khúc, một vài địa phương như bất động sản nghỉ dưỡng ở một số thị trường trọng điểm về du lịch hay phân khúc nhà ở cấp trung tại các thành phố lớn. Trong khi đó, thị trường lại thiếu nguồn cung của 1 số phân khúc khác như văn phòng cho thuê hạng A, đặc biệt là sự thiếu hụt văn phòng hạng A tại Tp.HCM hay nhà ở thu nhập thấp…

Thách thức thứ 3 là Chính phủ và Ngân hàng Nhà nước có các chính sách về kiểm soát tín dụng vào bất động sản. Việc dòng vốn tín dụng bị hạn chế sẽ khiến doanh nghiệp bất động sản - lĩnh vực cần nguồn vốn lớn gặp khó khăn trong việc huy động vốn.
"""

rewrite_ins = Rewrite()
print rewrite_ins.rewrite(txt)

