import os
import cPickle as pkl


def get_list_file_path(directory, l_except_file_name=['.DS_Store']):
    stripped_dir = directory.rstrip('/')
    return ['{}/{}'.format(stripped_dir, file_name) for file_name in os.listdir(stripped_dir) if file_name not in l_except_file_name]


def get_list_file_name(directory, l_except_file_name=['.DS_Store']):
    stripped_dir = directory.rstrip('/')
    return [file_name for file_name in os.listdir(stripped_dir) if file_name not in l_except_file_name]

with open('vocab.pkl', 'r') as f:
    d_vocab = pkl.load(f)

with open('count_matrix_reduce.pkl', 'r') as f:
    matrix = pkl.load(f)

i = 0
for vocab in d_vocab:
    print vocab
    print d_vocab[vocab]
    print matrix[0, d_vocab[vocab]]
    if i == 10:
        break

    i += 1

print len(d_vocab.keys())
