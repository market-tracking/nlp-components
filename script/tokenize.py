import os

from bs4 import BeautifulSoup
from pyvi import ViTokenizer

def get_list_file_path(directory, l_except_file_name=['.DS_Store']):
    stripped_dir = directory.rstrip('/')
    return ['{}/{}'.format(stripped_dir, file_name) for file_name in os.listdir(stripped_dir) if file_name not in l_except_file_name]


def get_list_file_name(directory, l_except_file_name=['.DS_Store']):
    stripped_dir = directory.rstrip('/')
    return [file_name for file_name in os.listdir(stripped_dir) if file_name not in l_except_file_name]

DIR = '/Users/nguyenhuutho/Downloads/input'
TARGET_DIR = '../test/'

l_file_path = get_list_file_path(DIR)
l_file_name = get_list_file_name(DIR)
for idx, file_path in enumerate(l_file_path):
    with open(file_path, 'r') as f:
        content = f.read()
        soup = BeautifulSoup(content, 'html.parser')
        raw_text = soup.getText().strip()
        
        l_tokenized_line = []
        l_line = raw_text.split('\n')
        for line in l_line:
            if line.strip() == '':
                continue

            l_tokenized_line.append(ViTokenizer.tokenize(line))

    with open(TARGET_DIR + '/' + l_file_name[idx], 'w+') as f:
        f.write('\n'.join(l_tokenized_line).encode('utf-8'))

    break
